from django.contrib import admin
from django.urls import path
from .views import TemplateHomeView

urlpatterns = [
    path('', TemplateHomeView.as_view(), name='home'),
]