from django.conf.global_settings import LOGIN_URL
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views.generic import TemplateView

class TemplateHomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['msg'] = 'Bem vindo ao Sistema'
        return context