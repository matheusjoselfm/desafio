from django.contrib import admin
from django.urls import path
from .views import signupview, userlist, userlogin, userlogout, password

urlpatterns = [
    path('registrar/', signupview, name="register"),
    path('', userlist, name="usuarios"),
    path('login/', userlogin, name="login"),
    path('deslogar/', userlogout, name="logout"),
    path('password', password, name="password"),
]