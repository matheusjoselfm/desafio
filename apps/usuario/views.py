#Django imports
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm, AdminPasswordChangeForm
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
#Local Imports
from .forms import SignupUserForm


LOGIN_URL = '/usuario/login/'

@login_required(login_url=LOGIN_URL)
def signupview(request):
    if request.method == 'POST':
        form = SignupUserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('usuarios')
    else:
        form = SignupUserForm

    return render(request, 'register.html', {'form':form})

@login_required(login_url=LOGIN_URL)
def userlist(request):
    context = {}
    usuarios = User.objects.all()
    context['usuarios'] = usuarios
    return render(request, 'list.html', context)

def userlogin(request):
    if request.method == 'POST':
        username_raw = request.POST['usuario']
        password_raw = request.POST['senha']
        user = authenticate(request, username=username_raw, password=password_raw)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/usuario/')
        else:
            return HttpResponseRedirect(LOGIN_URL)
    return render(request, 'login.html', {})

def userlogout(request):
    logout(request)
    return HttpResponseRedirect(LOGIN_URL)

@login_required
def password(request):
    if request.user.has_usable_password():
        PasswordForm = PasswordChangeForm
    else:
        PasswordForm = AdminPasswordChangeForm

    if request.method == 'POST':
        form = PasswordForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.success(request, 'Your password was successfully updated!')
            return redirect('home')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordForm(request.user)
    return render(request, 'password.html', {'form': form})