#Django Imports
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, ModelFormMixin, DeleteView
#Local Imports
from .models import Cliente

class ClienteView(CreateView):
    model = Cliente
    fields = ['nome', 'telefone', 'endereco', 'numero', 'cidade', 'estado', 'pais', 'cep']
    success_url = '/cliente/'

    def get_success_url(self):
        """Return the URL to redirect to after processing a valid form."""
        if self.success_url:
            url = self.success_url.format(**self.object.__dict__)
        else:
            try:
                url = self.object.get_absolute_url()
            except AttributeError:
                raise ImproperlyConfigured(
                    "No URL to redirect to.  Either provide a url or define"
                    " a get_absolute_url method on the Model.")
        return url

class ClienteListView(ListView):
    model = Cliente
    template_name = 'cliente_list.html'
    msg = 'Ficha de Cadastro'

    def get_context_data(self, *, object_list=None, Cliente=Cliente):
        context = {}
        context['object_list'] = Cliente.objects.all()
        return context

class ClienteUpdateView(UpdateView):
    model = Cliente
    fields = ['nome', 'telefone', 'endereco', 'numero', 'cidade', 'estado', 'pais', 'cep']
    success_url = '/cliente/'
    pk_url_kwarg = 'pk'

class ClienteDeleteView(DeleteView):
    model = Cliente
    success_url = '/cliente/'
    pk_url_kwarg = 'pk'