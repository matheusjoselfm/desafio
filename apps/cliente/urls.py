from django.contrib.auth.decorators import login_required
from django.urls import path
from .views import ClienteView, ClienteListView, ClienteUpdateView, ClienteDeleteView

urlpatterns = [
    path('criar', login_required(ClienteView.as_view()), name='cliente-criar'),
    path('', login_required(ClienteListView.as_view()), name="cliente-list"),
    path('editar/<int:pk>/', login_required(ClienteUpdateView.as_view()), name="cliente-edit"),
    path('excluir/<int:pk>/', login_required(ClienteDeleteView.as_view()), name="cliente-delete"),
]