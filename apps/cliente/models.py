from django.db import models

class Cliente(models.Model):
    nome = models.CharField(max_length=150, blank=False, null=False)
    telefone = models.CharField(max_length=15, blank=True, null=True)
    endereco = models.CharField(verbose_name="Endereço", max_length=150, blank=False, null=False)
    numero = models.IntegerField(verbose_name="Número", blank=True, null=True)
    cidade = models.CharField(max_length=100, blank=False, null=False)
    estado = models.CharField(max_length=100, blank=False, null=False)
    pais = models.CharField(verbose_name="País",max_length=100, blank=False, null=False)
    cep = models.CharField(max_length=10, blank=False, null=False)

    def __str__(self):
        return self.nome